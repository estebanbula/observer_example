package org.cursoreactividad.subscriber;

public interface Subscriber {

    void update(String value);
}
