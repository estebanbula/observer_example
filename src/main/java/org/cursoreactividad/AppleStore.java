package org.cursoreactividad;

import org.cursoreactividad.client.Client;
import org.cursoreactividad.publisher.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class AppleStore extends Publisher {

    private List<String> products;

    public AppleStore() {
        this.products = new ArrayList<>();
    }

    public void addNewProduct(String product) {
        products.add(product);
        System.out.println("New product has arrive!");
        this.notifyClient(product);
    }

    public List<String> getProducts() {
        return products;
    }

    @Override
    public void notifyClient(String product) {
        subscribers.forEach(subscriber -> subscriber.update(product));
    }

    public void unsubscribeByName(String name) {
        this.unsubscribeClient(subscribers
                .stream()
                .filter(client -> ((Client) client).getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("This client is not in the subscribers list")));
    }
}
