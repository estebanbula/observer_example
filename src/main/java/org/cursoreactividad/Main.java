package org.cursoreactividad;

import org.cursoreactividad.client.Client;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AppleStore appleStore = new AppleStore();
        storeOperation(appleStore);
    }

    private static void storeOperation(AppleStore appleStore) {
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        Scanner sc3 = new Scanner(System.in);

        System.out.println("" +
                "1. Subscribe client. \n" +
                "2. Unsubscribe client. \n" +
                "3. Publish new product. \n" +
                "0. Exit. ");
        int selectedOption = sc.nextInt();

        switch (selectedOption) {
            case 1:
                System.out.println("Subscribe client:");
                String name = sc2.nextLine();
                String email = sc3.nextLine();
                Client client = new Client(name, email);
                appleStore.subscribeClient(client);
                System.out.println("Subscription successful, client: " + client.getName());
                storeOperation(appleStore);
                break;

            case 2:
                System.out.println("Unsubscribe client:");
                appleStore.getClientsWithSubscription().forEach(System.out::println);
                String clientName = sc2.nextLine();
                appleStore.unsubscribeByName(clientName);
                System.out.println("Un-subscription successful, client: " + clientName);
                storeOperation(appleStore);
                break;

            case 3:
                System.out.println("Publish new product:");
                String product = sc2.nextLine();
                appleStore.addNewProduct(product);
                storeOperation(appleStore);
                break;
            case 8:
                System.out.println("Thanks for use our system.");
                break;
            default:
                System.out.println("Incorrect selection");
        }
    }
}