package org.cursoreactividad.client;

import org.cursoreactividad.subscriber.Subscriber;

public class Client implements Subscriber {

    private String name;
    private String email;

    public Client(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public void update(String value) {
        System.out.println(name + ", we have new products in stock: " + value);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
