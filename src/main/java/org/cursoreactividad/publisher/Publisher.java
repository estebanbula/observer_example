package org.cursoreactividad.publisher;

import org.cursoreactividad.subscriber.Subscriber;

import java.util.ArrayList;
import java.util.List;

public abstract class Publisher {

    protected List<Subscriber> subscribers;

    public Publisher() {
        this.subscribers = new ArrayList<>();
    }

    public void subscribeClient(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void unsubscribeClient(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    public abstract void notifyClient(String product);

    public List<Subscriber> getClientsWithSubscription() {
        return subscribers;
    }
}
